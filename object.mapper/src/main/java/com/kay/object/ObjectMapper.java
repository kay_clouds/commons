package com.kay.object;

import com.kay.object.assembler.Assembler;
import com.kay.object.assembler.ClassPropertyAssembler;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author lili.he
 * @param <FROM>
 * @param <TO>
 */
public class ObjectMapper<FROM, TO> implements Function<FROM, TO> {

    private static Map<String, Method> toMethodMap(String prefix, Class clazz) {
        return Arrays.stream(clazz.getMethods())
                .filter(method -> method.getName().startsWith(prefix))
                .collect(
                        Collectors.toMap(
                                //remove prfix, get property name as key
                                method -> method.getName().substring(prefix.length()),
                                // method as map value
                                method -> method
                        )
                );
    }

    private final Map<String, Assembler> propertyAssemblerMap;
    private final Class<FROM> fromClass;
    private final Class<TO> toClass;

    public ObjectMapper(Class<FROM> fromClass, Class<TO> toClass) {

        this.fromClass = fromClass;
        this.toClass = toClass;

        Map<String, Method> getterMap = toMethodMap("get", fromClass);
        Map<String, Method> setterMap = toMethodMap("set", fromClass);

        propertyAssemblerMap = new HashMap<>();

        setterMap
                .keySet()
                .forEach(name -> {
                    // if property is in the getter map
                    if (Objects.nonNull(getterMap.get(name))) {
                        // create a value assembler
                        propertyAssemblerMap.put(name,
                                new ClassPropertyAssembler(getterMap.get(name), setterMap.get(name))
                        );
                    }
                }
                );

    }

    @Override
    public TO apply(FROM from) {

        try {
            TO product = toClass.newInstance();
            propertyAssemblerMap.values().forEach(assembler -> assembler.assemble(from, product));
            return product;
        } catch (InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(ObjectMapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void addChildRuleByName(String name, Assembler propertyAssembler) {
        propertyAssemblerMap.put(name, propertyAssembler);
    }
}
