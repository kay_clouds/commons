package com.kay.object.assembler;

import static com.kay.object.util.Resolver.resolve;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 *
 * @author lili.he
 * @param <T>
 * @param <V>
 * @param <FROM>
 * @param <TO>
 */
public abstract class Assembler<T, V, FROM, TO> {

    protected Function<T, V> mapper;
    protected BiConsumer<TO, V> setter;
    protected Function<FROM, T> getter;
    protected BiConsumer<FROM, V> reprocessor = (FROM, V) -> {
        //do nothing for default
    };
    protected V empty;

    /**
     * Set assembly to product.
     *
     * @param assembly assembly to assemble
     * @param product product to assemble
     */
    public void injection(V assembly, TO product) {

        /**
         * assemble assembly to product.
         */
        setter.accept(product, assembly);
    }

    /**
     * Get value from material, transform value, and set to product.
     *
     * @param material
     * @param product
     */
    public void assemble(FROM material, TO product) {

        /**
         * Extract material.
         */
        Optional<T> materialExtracted = resolve(() -> getter.apply(material));
        /**
         * Transform material to assembly and set default empty value.
         */
        V assembly = materialExtracted.map(mapper).orElse(empty);

        /**
         * Reprocess with material.
         */
        reprocessor.accept(material, assembly);

        /**
         * assemble assembly to product.
         */
        setter.accept(product, assembly);
    }

}
