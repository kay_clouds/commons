package com.kay.object.assembler;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lili.he
 * @param <V>
 * @param <FROM>
 * @param <TO>
 */
public class ClassPropertyAssembler<V, FROM, TO> extends Assembler<V, V, FROM, TO> {

    public ClassPropertyAssembler(Method getterMethod, Method setterMethod) {

        super();
        mapper = v -> v;
        getter = from -> {
            try {
                return (V) getterMethod.invoke(from);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                Logger.getLogger(ClassPropertyAssembler.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        };
        setter = (to, value) -> {
            try {
                setterMethod.invoke(to, value);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                Logger.getLogger(ClassPropertyAssembler.class.getName()).log(Level.SEVERE, null, ex);
            }

        };

    }

}
