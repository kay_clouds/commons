package com.kay.object.assembler;

import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 *
 * @author Lili he
 * @param <T>
 * @param <V>
 * @param <FROM>
 * @param <TO>
 */
public class AssemblerBuilder<T, V, FROM, TO> {

    private Function<T, V> mapper;
    private BiConsumer<TO, V> setter;
    private Function<FROM, T> getter;
    private BiConsumer<FROM, V> reprocessor = (FROM from, V v) -> {
        //do nothing for default
    };

    private V empty;

    public AssemblerBuilder() {

    }

    public AssemblerBuilder<T, V, FROM, TO> childMapper(final Function<T, V> value) {
        this.mapper = value;
        return this;
    }

    public AssemblerBuilder<T, V, FROM, TO> toValueSetter(final BiConsumer<TO, V> value) {
        this.setter = value;
        return this;
    }

    public AssemblerBuilder<T, V, FROM, TO> fromValueGetter(final Function<FROM, T> value) {
        this.getter = value;
        return this;
    }

    public AssemblerBuilder<T, V, FROM, TO> reprocessor(final BiConsumer<FROM, V> value) {
        this.reprocessor = value;
        return this;
    }

    public AssemblerBuilder<T, V, FROM, TO> emptyIfError(final V value) {
        this.empty = value;
        return this;
    }

    public Assembler<T, V, FROM, TO> build() {
        Assembler<T, V, FROM, TO> assembler = new Assembler<T, V, FROM, TO>() {

        };
        assembler.mapper = mapper;
        assembler.setter = setter;
        assembler.getter = getter;
        assembler.reprocessor = reprocessor;
        assembler.empty = empty;
        return assembler;
    }

}
