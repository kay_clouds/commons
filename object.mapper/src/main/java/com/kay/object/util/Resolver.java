package com.kay.object.util;

import com.kay.object.ObjectMapper;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lili.he
 */
public class Resolver {

    public static <T> Optional<T> resolve(Supplier<T> resolver) {
        try {
            T result = resolver.get();
            return Optional.ofNullable(result);
        } catch (NullPointerException e) {
            Logger.getLogger(ObjectMapper.class.getName()).log(Level.SEVERE, null, e);
            return Optional.empty();
        }
    }

    public static <T> Optional<T> resolveAny(Supplier<T> resolver) {
        try {
            T result = resolver.get();
            return Optional.ofNullable(result);
        } catch (Exception e) {
            Logger.getLogger(ObjectMapper.class.getName()).log(Level.SEVERE, null, e);
            return Optional.empty();
        }
    }
}
